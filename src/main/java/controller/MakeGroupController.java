package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import dao.FCDao;

@WebServlet("/make-group")
public class MakeGroupController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Gson gson;

	public MakeGroupController() {
		super();
		this.gson = new Gson();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		FCDao fd = new FCDao();
		boolean validNumberOfTeam = fd.validNumberOfTeam();

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");

		if (!validNumberOfTeam) {
			response.getWriter().println(this.gson.toJson("Number of teams must be in range 8-16!"));
		} else {
			
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

}
