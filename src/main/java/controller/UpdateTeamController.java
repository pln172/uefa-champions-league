package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import dao.FCDao;
import model.FC;

@WebServlet("/update-team")
public class UpdateTeamController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Gson gson;

	public UpdateTeamController() {
		super();
		this.gson = new Gson();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));

		FCDao fd = new FCDao();
		FC fc = fd.getFCById(id);

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().println(this.gson.toJson(fc));
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		
	}

}
