package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import dao.FCDao;
import model.FC;

@WebServlet("/add-team")
public class AddTeamController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Gson gson;

	public AddTeamController() {
		super();
		this.gson = new Gson();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		FCDao fd = new FCDao();
		ArrayList<FC> FCs = fd.getFCs();

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().println(this.gson.toJson(FCs));
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		String team_name = request.getParameter("team_name");
		String coach_name = request.getParameter("coach_name");
		String national_name = request.getParameter("national_name");
		int seed = Integer.parseInt(request.getParameter("seed"));

		FC fc = new FC();
		fc.setId(id);
		fc.setTeam_name(team_name);
		fc.setCoach_name(coach_name);
		fc.setNational_name(national_name);
		fc.setSeed(seed);

		FCDao fd = new FCDao();
		if (fd.dupplicatedFCName(team_name)) {
			fd.updateFC(fc);
		} else {
			fd.addFC(fc);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().println(this.gson.toJson("success"));
		}

	}

}
