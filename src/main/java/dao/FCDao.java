package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import model.FC;
import utils.ConnectionDB;

public class FCDao {

	public ArrayList<FC> getFCs() {
		ArrayList<FC> FCs = new ArrayList();
		FC fc;

		String sql = "SELECT [ID]\n" + "      ,[Team name]\n" + "      ,[Coach name]\n" + "      ,[National name]\n"
				+ "      ,[Seed]\n" + "  FROM [FC]";

		try {
			Connection con = ConnectionDB.getConnection();
			PreparedStatement stm = con.prepareStatement(sql);
			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				fc = new FC();
				fc.setId(rs.getInt("ID"));
				fc.setTeam_name(rs.getString("Team name"));
				fc.setCoach_name(rs.getString("Coach name"));
				fc.setNational_name(rs.getString("National name"));
				fc.setSeed(rs.getInt("Seed"));

				FCs.add(fc);
			}
			return FCs;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return null;
	}

	public FC getFCById(int id) {
		FC fc;
		String sql = "SELECT [ID]\n" + "      ,[Team name]\n" + "      ,[Coach name]\n" + "      ,[National name]\n"
				+ "      ,[Seed]\n" + "  FROM [FC]\n" + "  WHERE ID = ?";

		try {
			Connection con = ConnectionDB.getConnection();
			PreparedStatement stm = con.prepareStatement(sql);
			stm.setInt(1, id);
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				fc = new FC();
				fc.setTeam_name(rs.getString("Team name"));
				fc.setId(rs.getInt(id));
				fc.setCoach_name(rs.getString("Coach name"));
				fc.setNational_name(rs.getString("National name"));
				fc.setSeed(rs.getInt("Seed"));

				return fc;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public boolean dupplicatedFCName(String teamName) {
		String sql = "SELECT [ID]\n" + "      ,[Team name]\n" + "      ,[Coach name]\n" + "      ,[National name]\n"
				+ "      ,[Seed]\n" + "  FROM [FC]\n" + "  WHERE [Team name] like ?";

		try {
			Connection con = ConnectionDB.getConnection();
			PreparedStatement stm = con.prepareStatement(sql);
			stm.setString(1, teamName);
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return false;
	}

	public void addFC(FC fc) {
		String sql = "INSERT INTO [FC]\n" + "           ([Team name]\n" + "           ,[Coach name]\n"
				+ "           ,[National name]\n" + "           ,[Seed])\n" + "     VALUES\n" + "           (?\n"
				+ "           ,?\n" + "           ,?\n" + "           ,?)";

		try {
			Connection con = ConnectionDB.getConnection();
			PreparedStatement stm = con.prepareStatement(sql);

			int i = 1;
			stm.setString(i++, fc.getTeam_name());
			stm.setString(i++, fc.getCoach_name());
			stm.setString(i++, fc.getNational_name());
			stm.setInt(i++, fc.getSeed());

			stm.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void updateFC(FC fc) {
		String sql = "UPDATE [FC]\n" + "   SET [Team name] = ?\n" + "      ,[Coach name] = ?\n"
				+ "      ,[National name] = ?\n" + "      ,[Seed] = ?\n" + " WHERE ID = ? OR [Team name] like ?";

		try {
			Connection con = ConnectionDB.getConnection();
			PreparedStatement stm = con.prepareStatement(sql);

			int i = 1;
			stm.setString(i++, fc.getTeam_name());
			stm.setString(i++, fc.getCoach_name());
			stm.setString(i++, fc.getNational_name());
			stm.setInt(i++, fc.getSeed());
			stm.setInt(i++, fc.getId());
			stm.setString(i++, fc.getTeam_name());

			stm.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void deleteFC(int id) {
		String sql = "DELETE FROM [FC]\n" + "      WHERE ID = ?";

		try {
			Connection con = ConnectionDB.getConnection();
			PreparedStatement stm = con.prepareStatement(sql);
			stm.setInt(1, id);

			stm.executeUpdate();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public boolean validNumberOfTeam() {
		String sql = "SELECT count([ID]) as NumberOfTeams\n" + "  FROM [FC]";

		try {
			Connection con = ConnectionDB.getConnection();
			PreparedStatement stm = con.prepareStatement(sql);
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				if (rs.getInt("NumberOfTeams") < 8 || rs.getInt("NumberOfTeams") > 16)
					return false;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return true;
	}
}
