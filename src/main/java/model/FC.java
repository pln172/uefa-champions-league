package model;

public class FC {
	private int id;
	private String team_name;
	private String coach_name;
	private String national_name;
	private int seed;

	public FC() {
		super();
	}

	public FC(int id, String team_name, String coach_name, String national_name, int seed) {
		super();
		this.id = id;
		this.team_name = team_name;
		this.coach_name = coach_name;
		this.national_name = national_name;
		this.seed = seed;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTeam_name() {
		return team_name;
	}

	public void setTeam_name(String team_name) {
		this.team_name = team_name;
	}

	public String getCoach_name() {
		return coach_name;
	}

	public void setCoach_name(String coach_name) {
		this.coach_name = coach_name;
	}

	public String getNational_name() {
		return national_name;
	}

	public void setNational_name(String national_name) {
		this.national_name = national_name;
	}

	public int getSeed() {
		return seed;
	}

	public void setSeed(int seed) {
		this.seed = seed;
	}

}
