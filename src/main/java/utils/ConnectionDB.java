package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDB {
	private static final String driverName = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	private static final String connectionString = "jdbc:sqlserver://localhost;databaseName=UEFA CHAMPIONS LEAGUE";
	private static final String username = "sa";
	private static final String password = "sa";

	public static Connection getConnection() {
		try {
			Class.forName(driverName);
			return DriverManager.getConnection(connectionString, username, password);

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static void disconnect(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
